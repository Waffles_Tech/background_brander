from __future__ import print_function
import os
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
import netifaces as ni
import platform
from datetime import timedelta
import psutil
from collections import OrderedDict
import pprint

script_file_path = "/path/to/directory/"
background_original = "original_image_name.png"
finished_file_name = "modified_image_name.png"

interface_ip_list = []

for interface in ni.interfaces():
	if interface == "lo":
		pass
	else:
		ip = ni.ifaddresses(interface)[ni.AF_INET][0]['addr']
		interface_ip_list.append(interface + ":  " + ip)

image_write_list = []
for thing in interface_ip_list:
	image_write_list.append(thing)

with open('/proc/uptime', 'r') as f:
	uptime_seconds = float(f.readline().split()[0])
	uptime_thing = str(timedelta(seconds = uptime_seconds))
	uptime_split = uptime_thing.split(".")
	uptime_string = uptime_split[0]

mem_stuff = psutil.virtual_memory()
mem_used = int(int(mem_stuff.used) / 1024) / 1024
mem_total = int(int(mem_stuff.total) / 1024) / 1024
mem_used_total = str(mem_used) + " MiB / " + str(mem_total) + " MiB"

def cpuinfo():
        cpuinfo=OrderedDict()
        procinfo=OrderedDict()
        nprocs = 0
        with open('/proc/cpuinfo') as f:
                for line in f:
                        if not line.strip():
                                cpuinfo['proc%s' % nprocs] = procinfo
                                nprocs=nprocs+1
                                procinfo=OrderedDict()
                        else:
                                if len(line.split(':')) == 2:
                                        procinfo[line.split(':')[0].strip()] = line.split(':')[1].strip()
                                else:
                                        procinfo[line.split(':')[0].strip()] = ''
        return cpuinfo

cpuinfo = cpuinfo()
cpu_list = []

for processor in cpuinfo.keys():
        cpu_list.append(cpuinfo[processor]['model name'])

cpu_str = str(cpu_list[0]) + "  (" + str(len(cpu_list)) + " Cores)"
cpu_info = cpu_str.replace("(R)", "").replace("(TM)", "")

image_write_list.append("Hostname:  " + platform.node())
image_write_list.append("OS:  " + platform.system())
image_write_list.append("Kernel:  " + platform.release())
image_write_list.append("Uptime:  " + uptime_string)
image_write_list.append("CPU:  " + cpu_info)
image_write_list.append("RAM:  " + mem_used_total)

photo = Image.open(script_file_path + background_original)
drawing = ImageDraw.Draw(photo)
black = 3, 8, 12
font = ImageFont.truetype("/usr/share/fonts/truetype/lato/Lato-Black.ttf", 15)
x = 60
y = 300

fillcolor = "white"

for item in image_write_list:
	drawing.text((x, y), item, font=font, fill=fillcolor)
	y = y + 20

photo.save(script_file_path + finished_file_name)

os.system("/usr/bin/gsettings set org.gnome.desktop.background picture-uri " + script_file_path + finished_file_name)

